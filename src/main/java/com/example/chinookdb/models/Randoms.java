package com.example.chinookdb.models;

import java.util.ArrayList;

public class Randoms {
    private String artist;
    private String track;
    private String genre;

    public Randoms(String artist, String track, String genre) {
        this.artist = artist;
        this.track = track;
        this.genre = genre;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getTrack() {
        return track;
    }

    public void setTracks(String track) {
        this.track = track;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenres(String genres) {
        this.genre = genres;
    }
}
