package com.example.chinookdb.models;

public class CustomerGenre {
    private String genreName;
    private int tracks;

    public CustomerGenre(String genreName, int tracks) {
        this.genreName = genreName;
        this.tracks = tracks;
    }

    public String getGenreName() {
        return genreName;
    }

    public void setGenreName(String genreName) {
        this.genreName = genreName;
    }

    public int getTracks() {
        return tracks;
    }

    public void setTracks(int tracks) {
        this.tracks = tracks;
    }
}
