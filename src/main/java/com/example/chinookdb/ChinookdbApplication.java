package com.example.chinookdb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChinookdbApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChinookdbApplication.class, args);
	}

}
