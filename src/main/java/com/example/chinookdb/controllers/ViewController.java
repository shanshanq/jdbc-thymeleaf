package com.example.chinookdb.controllers;

import com.example.chinookdb.data_access.CustomerRepository;

import com.example.chinookdb.models.Track;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ViewController {
    private final CustomerRepository customerRepository;

    public ViewController(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(Model model){
        model.addAttribute("random", customerRepository.getRandom());
        return "index";
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String search(@RequestParam(value="term", defaultValue = "123") String term, Model model){
        model.addAttribute("tracks", customerRepository.search(term));
        return "view-search-result" ;
    }



}
