package com.example.chinookdb.controllers;

import com.example.chinookdb.data_access.CustomerRepository;
import com.example.chinookdb.models.*;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class CustomerController {
    private final CustomerRepository customerRepository;

    public CustomerController(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @RequestMapping(value = "api/customers", method = RequestMethod.GET)
    public ArrayList<Customer> getAllCustomers(){
        return customerRepository.getAllCustomers();
    }

    @RequestMapping(value = "api/customers/{id}", method = RequestMethod.GET)
    public Customer getCustomerById(@PathVariable String id){
        return customerRepository.getCustomerById(id);
    }

    @RequestMapping(value = "api/customer/{name}", method = RequestMethod.GET)
    public Customer getCustomerByName(@PathVariable String name){
        return customerRepository.getCustomerByName(name);
    }

    @RequestMapping(value = "api/customers/page", method = RequestMethod.GET)
    public ArrayList<Customer> getCustomers(@RequestParam(value="limit", defaultValue = "10") String limit, @RequestParam(value="offset", defaultValue = "0") String offset){
        return customerRepository.getCustomers(limit,offset);
    }

    @RequestMapping(value = "api/customers", method = RequestMethod.POST)
    public Boolean addCustomer(@RequestBody Customer customer){
        return customerRepository.addCustomer(customer);
    }

    @RequestMapping(value = "api/customers/{id}", method = RequestMethod.PUT)
    public Boolean updateCustomer(@PathVariable String id, @RequestBody Customer customer){
        return customerRepository.updateCustomer(customer);
    }

    @RequestMapping(value = "api/customers/country", method = RequestMethod.GET)
    public ArrayList<CustomerCountry> getTheNumberOfCustomersByCountry(){
        return customerRepository.getTheNumberOfCustomersByCountry();
    }

    @RequestMapping(value = "api/customers/spender", method = RequestMethod.GET)
    public ArrayList<CustomerSpender> getHighestSpenders(){
        return customerRepository.getHighestSpenders();
    }

    @RequestMapping(value = "api/customers/{id}/genre", method = RequestMethod.GET)
    public ArrayList<CustomerGenre> getTheMostPopularGenre(@PathVariable String id){
        return customerRepository.getTheMostPopularGenre(id);
    }


//    @RequestMapping(value = "/search", method = RequestMethod.GET)
//    public Track search(@RequestParam(value="term", defaultValue = "123") String term){
//        return customerRepository.search(term) ;
//   }

//    @RequestMapping(value = "/random", method = RequestMethod.GET)
//      public ArrayList<Randoms> getRandom(){
//        return customerRepository.getRandom() ;
//   }


}
