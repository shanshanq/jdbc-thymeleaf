package com.example.chinookdb.data_access;

import com.example.chinookdb.logging.LogToConsole;
import com.example.chinookdb.models.*;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

@Repository
public class CustomerRepository {
    private final LogToConsole logger;
    private String URL = ConnectionHelper.CONNECTION_URL;
    private Connection conn = null;



    public CustomerRepository(LogToConsole logger) {
        this.logger = logger;
    }


    public ArrayList<Customer> getAllCustomers(){
        ArrayList<Customer> customers = new ArrayList<>();
        try{
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            // Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM customer");
            // Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customers.add(
                        new Customer(
                                resultSet.getInt("CustomerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getString("Country"),
                                resultSet.getString("PostalCode"),
                                resultSet.getString("Phone"),
                                resultSet.getString("Email")
                        ));
            }
            logger.log("Select all customers successful");
        }
        catch (Exception exception){
            logger.log(exception.toString());
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                logger.log(exception.toString());
            }
        }
        return customers;
    }

    public Customer getCustomerById(String cId){
        Customer customer = null;
        try{
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            // Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM customer WHERE CustomerId = ?");
            preparedStatement.setString(1, cId);
            // Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customer = new Customer(
                        resultSet.getInt("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Country"),
                        resultSet.getString("PostalCode"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                );
            }
            logger.log("Select specific customer successful");
        }
        catch (Exception exception){
            logger.log(exception.toString());
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                logger.log(exception.toString());
            }
        }
        return customer;
    }

    public Customer getCustomerByName(String name){
        Customer customer = null;
        try{
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            // Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM customer WHERE FirstName OR LastName LIKE ?");
            preparedStatement.setString(1, '%'+ name +'%');
            // Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customer = new Customer(
                        resultSet.getInt("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Country"),
                        resultSet.getString("PostalCode"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                );
            }
            logger.log("Select specific customer successful");
        }
        catch (Exception exception){
            logger.log(exception.toString());
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                logger.log(exception.toString());
            }
        }
        return customer;
    }

    public ArrayList<Customer> getCustomers(String limit, String offset){
        ArrayList<Customer> customers = new ArrayList<>();
        try{
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            // Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM customer LIMIT ? OFFSET ?");
            preparedStatement.setString(1, limit);
            preparedStatement.setString(2, offset);
            // Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customers.add(
                        new Customer(
                                resultSet.getInt("CustomerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getString("Country"),
                                resultSet.getString("PostalCode"),
                                resultSet.getString("Phone"),
                                resultSet.getString("Email")
                        ));
            }
            logger.log("Select a page of customers successful");
        }
        catch (Exception exception){
            logger.log(exception.toString());
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                logger.log(exception.toString());
            }
        }
        return customers;
    }

    public Boolean addCustomer(Customer customer){
        Boolean success = false;
        try{
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            // Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("INSERT INTO customer(CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email) VALUES(?,?,?,?,?,?,?)");
            preparedStatement.setInt(1,customer.getCustomerId());
            preparedStatement.setString(2,customer.getFirstName());
            preparedStatement.setString(3,customer.getLastName());
            preparedStatement.setString(4,customer.getCountry());
            preparedStatement.setString(5, customer.getPostalCode());
            preparedStatement.setString(6, customer.getPhone());
            preparedStatement.setString(7, customer.getEmail());
            // Execute Query
            int result = preparedStatement.executeUpdate();
            success = (result != 0);
            logger.log("Add customer successful");
        }
        catch (Exception exception){
            logger.log(exception.toString());
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                logger.log(exception.toString());
            }
        }
        return success;
    }

    public Boolean updateCustomer(Customer customer){
        Boolean success = false;
        try{
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            // Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("UPDATE customer SET CustomerId = ?, FirstName = ?, LastName = ?, Country = ?, PostalCode = ?, Phone = ?, Email = ? WHERE CustomerId=?");
            preparedStatement.setInt(1,customer.getCustomerId());
            preparedStatement.setString(2,customer.getFirstName());
            preparedStatement.setString(3,customer.getLastName());
            preparedStatement.setString(4,customer.getCountry());
            preparedStatement.setString(5,customer.getPostalCode());
            preparedStatement.setString(6,customer.getPhone());
            preparedStatement.setString(7,customer.getEmail());
            preparedStatement.setInt(8,customer.getCustomerId());
            // Execute Query
            int result = preparedStatement.executeUpdate();
            success = (result != 0);
            logger.log("Update customer successful");
        }
        catch (Exception exception){
            logger.log(exception.toString());
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                logger.log(exception.toString());
            }
        }
        return success;
    }

    public ArrayList<CustomerCountry> getTheNumberOfCustomersByCountry(){
        ArrayList<CustomerCountry> list = new ArrayList<>();
        try{
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            // Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT Country, count(*) AS Count FROM customer GROUP BY Country ORDER BY Count DESC");
            // Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                list.add(
                        new CustomerCountry(
                                resultSet.getString("Country"),
                                resultSet.getInt("Count")
                        ));
            }
            logger.log("Select the countries and the counts successful");
        }
        catch (Exception exception){
            logger.log(exception.toString());
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                logger.log(exception.toString());
            }
        }
        return list;
    }

    public ArrayList<CustomerSpender> getHighestSpenders(){
        ArrayList<CustomerSpender> spenderList = new ArrayList<>();
        try{
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            // Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT customer.CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email, invoice.Total FROM customer INNER JOIN invoice WHERE invoice.CustomerId = customer.CustomerId ORDER BY Total DESC");
            // Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                spenderList.add(
                        new CustomerSpender(
                                resultSet.getInt("CustomerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getString("Country"),
                                resultSet.getString("PostalCode"),
                                resultSet.getString("Phone"),
                                resultSet.getString("Email"),
                                resultSet.getBigDecimal("Total")
                        ));
            }
            logger.log("Get highest spenders successful");
        }
        catch (Exception exception){
            logger.log(exception.toString());
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                logger.log(exception.toString());
            }
        }
        return spenderList;
    }

    public ArrayList<CustomerGenre> getTheMostPopularGenre(String id){
        ArrayList<CustomerGenre> list = new ArrayList<>();
        try{
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            // Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT Genre, MAX(TracksCount) AS TracksCount FROM (SELECT genre.Name AS Genre, COUNT(track.trackId) AS TracksCount FROM track INNER JOIN genre ON track.GenreId = genre.GenreId INNER JOIN invoiceLine ON track.TrackId = invoiceLine.TrackId INNER JOIN invoice ON invoiceLine.InvoiceId = invoice.InvoiceId INNER JOIN customer on invoice.CustomerId = customer.CustomerId WHERE customer.CustomerId = ? GROUP BY genre.GenreId) "
                            );
            preparedStatement.setString(1, id);
            // Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                list.add(
                        new CustomerGenre(
                                resultSet.getString("Genre"),
                                resultSet.getInt("TracksCount")
                        ));
            }
            logger.log("Get the most popular genre successful");
        }
        catch (Exception exception){
            logger.log(exception.toString());
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                logger.log(exception.toString());
            }
        }
        return list;
    }

    public ArrayList<Track> search(String term){
        ArrayList <Track> tracks = new ArrayList<>();
        try{
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            // Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT track.Name AS TrackName, artist.Name AS Artist, album.Title AS Album, genre.Name AS Genre FROM track INNER JOIN genre ON track.GenreId = genre.GenreId INNER JOIN album ON track.AlbumId = album.AlbumId INNER JOIN artist ON artist.ArtistId = album.AlbumId WHERE track.Name LIKE ?");
            preparedStatement.setString(1, '%'+ term +'%');
            // Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                tracks.add(new Track(
                        resultSet.getString("TrackName"),
                        resultSet.getString("Artist"),
                        resultSet.getString("Album"),
                        resultSet.getString("Genre")
                ));
            }
        }
        catch (Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return tracks;

}
     public ArrayList<Randoms> getRandom(){
         ArrayList<Randoms> list = new ArrayList<>();
         try{
             // Connect to DB
             conn = DriverManager.getConnection(URL);
             // Make SQL query
             PreparedStatement preparedStatement =
                     conn.prepareStatement("SELECT track.Name AS TrackName, artist.Name AS Artist, genre.Name AS Genre FROM track JOIN genre JOIN artist ORDER BY RANDOM() LIMIT 5");
             // Execute Query
             ResultSet resultSet = preparedStatement.executeQuery();

             while (resultSet.next()) {
                 list.add(
                         new Randoms(
                                 resultSet.getString("Artist"),
                                 resultSet.getString("TrackName"),
                                 resultSet.getString("Genre")
                         ));
             }
         }
         catch (Exception exception){
             System.out.println(exception.toString());
         }
         finally {
             try {
                 conn.close();
             }
             catch (Exception exception){
                 System.out.println(exception.toString());
             }
         }
         return list;
     }






}
