FROM openjdk:16
ADD target/chinookdb-0.0.1-SNAPSHOT.jar  jdbc-tl.jar
ENTRYPOINT [ "java", "-jar", "/jdbc-tl.jar" ]